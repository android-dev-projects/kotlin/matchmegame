package tk.monady.matchmegame.models

data class GameCard(
    val id: Int,
    var clicked: Boolean = false,
    val imagePath: String,
    val name: String,

)
