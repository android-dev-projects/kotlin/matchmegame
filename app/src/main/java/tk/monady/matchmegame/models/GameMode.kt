package tk.monady.matchmegame.models

enum class GameMode {
    CountDown, CountUp
}