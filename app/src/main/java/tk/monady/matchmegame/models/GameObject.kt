package tk.monady.matchmegame.models

data class GameObject(
    val id: Int,
    var score: Int = 0,
    var gameMode: GameMode,
    var timeStamp: String
)

