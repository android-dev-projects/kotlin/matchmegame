package tk.monady.matchmegame.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

class GameViewModel(private val app: Application): AndroidViewModel(app) {
    private val job = Job()
    private val scope = CoroutineScope(job + Dispatchers.IO)

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}

